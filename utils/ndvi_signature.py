import os
import sys
import torch
import matplotlib.pyplot as plt
import numpy as np
from scipy.ndimage.filters import maximum_filter1d

# from utils.sentinel_dataset import SentinelDataset
from torch import float64


def __moving_average(x, w):
    return np.convolve(x, np.ones(w), 'same') / w


def __max_filter1d_valid(a, w):
    b = a.clip(min=0)  # transform negative elements to zero
    return maximum_filter1d(b, size=w)


def get_all_signatures(inp, target, num_cls, b4_index, b8_index):
    """
    expected input having shape  (c, t, h, w) and target (h, w)
        c = number of channels for each sentinel-2 image
        t = number of images in the time series
        hxw = image size
    """
    c, t, h, w = inp.shape
    output_ndvi = np.zeros((t, h, w), dtype=np.float)

    # xin = torch.linspace(1, t, t)

    for cls_index_ in range(0, num_cls):
        pts = (target == cls_index_).numpy()
        all_ndvi_x_cls = []
        for row, yr in enumerate(pts):
            for col, xc in enumerate(yr):
                if xc:  # is True
                    # if target[batch_index_, row, col].item() != cls_index_:
                    #     print("error")
                    b8 = inp[b8_index, :, row, col]
                    b4 = inp[b4_index, :, row, col]
                    ndvi = (b8 - b4) / (b8 + b4)
                    ndvi = np.nan_to_num(ndvi.numpy())
                    # if np.isnan(ndvi).any():
                    #     print("NAN in ndvi!")
                    all_ndvi_x_cls.append(ndvi)
        mean_ndvi = np.zeros((t,), dtype=float)
        if len(all_ndvi_x_cls) > 1:
            mean_ndvi = np.mean(all_ndvi_x_cls, axis=0)
        if len(all_ndvi_x_cls) == 1:
            mean_ndvi = all_ndvi_x_cls[0]
        mmax_ndvi = __max_filter1d_valid(mean_ndvi, 5)  # moving max x class

        # print("batch", batch_index_, ", cls", cls_index_, ", ndvi", mmax_ndvi)
        # plt.plot(xin, mmax_ndvi)

        output_ndvi[:, pts] = mmax_ndvi.reshape(t, 1)
    # plt.show()
    return torch.from_numpy(output_ndvi).float()


def class_activations(features, weights, cls_index, batch, cx, cy):
    '''
    return tensor with 30 elements, one for each sample in the time series.
    '''
    with torch.no_grad():
        # suppose a kernel size 3x3
        m = torch.nn.ZeroPad2d(1)
        f1 = m(features)[batch, :, cy:cy+3, cx:cx+3]
        w1 = weights[cls_index, :, :, :]
        mul = w1*f1
        # reduce dimensions 1 and 2 and preserve dim 0 (time series)
        sum = torch.sum(mul, (1, 2))
        return sum


def write_signatures(fcn, target, output, target_ndvi, out_ndvi, dates, patch_id, opt, set_name):
    # target_ndvi = torch.randint(0, nc, size=(b, s, h, w))
    # patch_id = ('11', '12')
    # dates = [('20200101', '20200301'), ('20200102', '20200302'), ('20200103', '20200303'), ('20200104', '20200304'),
    #          ('20200105', '20200305')]
    nb, nt, h, w = target_ndvi.shape
    assert(len(dates) == nt)
    assert(len(dates[0]) == nb)

    # winner class for each pixel
    winners = torch.softmax(output, dim=1).argmax(dim=1)

    # bias = fcn.conv5out.bias
    weight = fcn.conv5out.weight

    for idx, patch_name in enumerate(patch_id):
        # print('patch_name:', patch_name)
        with open(os.path.join(opt.result_path, set_name + '_patch_' + patch_name + ".txt"), 'w') as f:
            f.write('class, output, type_ndvi, x, y, ')
            f.write(', '.join(map(str, [e[idx] for e in dates])))
            f.write("\n")
            for y in range(0, h):
                for x in range(0, w):
                    target_idx = target[idx, y, x].item()
                    output_idx = winners[idx, y, x].item()
                    f.write('%d, %d, target, %d, %d, ' % (target_idx, output_idx, x, y))
                    f.write(', '.join(map(str, target_ndvi[idx, :, y, x].data.tolist())))
                    f.write("\n")
                    f.write('%d, %d, predic, %d, %d, ' % (target_idx, output_idx, x, y))
                    f.write(', '.join(map(str, out_ndvi[idx, :, y, x].data.tolist())))
                    f.write("\n")
                    f.write('%d, %d, cls_ai, output, %d, %d, ' % (target_idx, output_idx, x, y))
                    cai = class_activations(out_ndvi, weight, target_idx, idx, y, x)
                    f.write(', '.join(map(str, cai.data.tolist())))
                    f.write("\n")
            # f.write("\n")


def plot_signatures(inp, target, cls_index, b4_index, b8_index):
    xin = torch.linspace(1, inp.shape[2], inp.shape[2])
    sample = inp.shape[0]//2  # intermediate sample
    pts = target[sample] == cls_index
    # All NDVI of the same class inside the same input image
    all_ndvi_x_cls = []
    for row, yr in enumerate(pts.numpy()):
        for col, xc in enumerate(yr):
            if xc:  # is True
                if target[sample, row, col].item() != cls_index:
                    print("error")
                b8 = inp[sample, b8_index, :, row, col]
                b4 = inp[sample, b4_index, :, row, col]
                ndvi = (b8 - b4) / (b8 + b4)
                all_ndvi_x_cls.append(ndvi.numpy())
                plt.plot(xin, ndvi)
    if len(all_ndvi_x_cls) > 1:
        mean_ndvi = np.mean(all_ndvi_x_cls, axis=0)
        # plt.plot(xin, mean_ndvi, color='black', linestyle='dashed', linewidth=4)
        # mmean_ndvi = moving_average(mean_ndvi, 5)  # moving average
        # plt.plot(xin, mmean_ndvi, color='red', linestyle='dashed', linewidth=4)
        mmax_ndvi = __max_filter1d_valid(mean_ndvi, 5)  # moving max
        plt.plot(xin, mmax_ndvi, color='red', linestyle='dashed', linewidth=4)
        plt.title('Class index ' + str(cls_index))
        plt.xlabel('time series')
        plt.ylabel('NDVI')
        plt.show()


# def real_visual_test():
#     dataset = SentinelDataset("/home/superior/datasets-nas/sentinel2/munich480",
#                               tileids="tileids/test_fold0.tileids",
#                               seqlength=30)
#
#     # dataset = SentinelDataset("/home/superior/datasets-nas/sentinel2/IREA/lombardia",
#     #                           tileids="tileids/train_fold0.tileids",
#     #                           seqlength=30)
#
#     dataloader = torch.utils.data.DataLoader(dataset, batch_size=4, shuffle=True, num_workers=1)
#
#     b4_index_ = dataset.b4_index
#     b8_index_ = dataset.b8_index
#
#     # (c x t x h x w)
#     # inp_ = torch.rand(2, 13, 30, 48, 48)
#     # target_ = torch.randint(9, (2, 48, 48))
#     while True:
#         inp_, target_ = next(iter(dataloader))
#         cls_index_ = 1
#         for cls_index_ in range(0, 17):
#             plot_signatures(inp_, target_, cls_index_, b4_index_, b8_index_)
#
#         input('press return to continue')


def __fake_visual_test():
    b4_index_ = 2
    b8_index_ = 3

    # (c x t x h x w)
    inp_ = torch.rand(2, 13, 30, 48, 48)
    target_ = torch.randint(9, (2, 48, 48))
    cls_index_ = 0
    plot_signatures(inp_, target_, cls_index_, b4_index_, b8_index_)


def __test():
    b4_index_ = 2
    b8_index_ = 3
    num_cls = 9

    # (c x t x h x w)
    inp_ = torch.rand(13, 30, 48, 48)
    target_ = torch.randint(num_cls, (48, 48))

    get_all_signatures(inp_, target_, num_cls, b4_index_, b8_index_)


# def _test2():
#     dataset = SentinelDataset("/home/superior/datasets-nas/sentinel2/munich480",
#                               tileids="tileids/test_fold0.tileids",
#                               seqlength=30)
#
#     # dataset = SentinelDataset("/home/superior/datasets-nas/sentinel2/IREA/lombardia",
#     #                           tileids="tileids/train_fold0.tileids",
#     #                           seqlength=30)
#
#     dataloader = torch.utils.data.DataLoader(dataset, batch_size=2, shuffle=True, num_workers=1)
#
#     b4_index_ = dataset.b4_index
#     b8_index_ = dataset.b8_index
#     num_cls = len(dataset.classes)
#
#     # (c x t x h x w)
#     inp_, target_ = next(iter(dataloader))
#
#     get_all_signatures(inp_, target_, num_cls, b4_index_, b8_index_)


if __name__ == "__main__":
    # real_visual_test()
    # _fake_visual_test()
    __test()
    # _test2()

    exit(0)
