'''FPN in PyTorch.
See the paper "Feature Pyramid Networks for Object Detection" for more details.
'''
import os

import torch
import torch.nn as nn
import torch.nn.functional as F

from models.model import generate_model
from opts import parse_opts
from utils.sentinel_dataset import RandomDataset


# from model.backbone import build_backbone


# def build_backbone(back_bone):
#     if back_bone == "resnet101":
#         return ResNet101(pretrained=True)


class Bottleneck(nn.Module):
    expansion = 4

    def __init__(self, in_planes, planes, stride=1):
        super(Bottleneck, self).__init__()
        self.conv1 = nn.Conv2d(in_planes, planes, kernel_size=1, stride=stride, bias=False)
        self.bn1 = nn.BatchNorm2d(planes)
        self.conv2 = nn.Conv2d(planes, planes, kernel_size=3, stride=stride, padding=1, bias=False)
        self.bn2 = nn.BatchNorm2d(planes)
        self.conv3 = nn.Conv2d(planes, self.expansion * planes, kernel_size=1, bias=False)
        self.bn3 = nn.BatchNorm2d(self.expansion * planes)

        self.shortcut = nn.Sequential()
        if stride != 1 or in_planes != self.expansion * planes:
            self.shortcut = nn.Sequential(
                nn.Conv2d(in_planes, self.expansion * planes, kernel_size=1, stride=stride, bias=False),
                nn.BatchNorm2d(self.expansion * planes)
            )

    def forward(self, x):
        out = F.relu(self.bn1(self.conv1(x)))
        out = F.relu(self.bn2(self.conv2(out)))
        out = self.bn3(self.conv3(out))
        out += self.shortcut(x)
        out = F.relu(out)
        return out


class FPN(nn.Module):

    def __init__(self, opt, first_batch):
        super(FPN, self).__init__()
        # self.first_run = True
        self.in_planes = 64
        self.num_classes = opt.n_classes

        # self.conv1 = nn.Conv2d(opt.sample_duration, 64, kernel_size=7, stride=2, padding=3, bias=False)
        # self.bn1 = nn.BatchNorm2d(64)

        # self.back_bone = build_backbone(back_bone)
        self.back_bone, parameters = generate_model(opt)
        # print("Back bone:\n", self.back_bone)

        # Bottom-up layers
        # self.layer1 = self._make_layer(Bottleneck, 64, num_blocks[0], stride=1)
        # self.layer2 = self._make_layer(Bottleneck, 128, num_blocks[1], stride=2)
        # self.layer3 = self._make_layer(Bottleneck, 256, num_blocks[2], stride=2)
        # self.layer4 = self._make_layer(Bottleneck, 512, num_blocks[3], stride=2)

        # Top layer
        self.toplayer = None  # nn.Conv3d(512, 256, kernel_size=1, stride=1, padding=0)  # Reduce channels

        # Smooth layers
        # self.smooth1 = None  # nn.Conv3d(512, 256, kernel_size=3, stride=1, padding=1)
        # self.smooth2 = None  # nn.Conv3d(512, 256, kernel_size=3, stride=1, padding=1)
        # self.smooth3 = None  # nn.Conv3d(512, 256, kernel_size=3, stride=1, padding=1)

        # Lateral layers
        self.latlayer1 = None  # nn.Conv3d(256, 256, kernel_size=1, stride=1, padding=0)
        self.latlayer2 = None  # nn.Conv3d(128, 256, kernel_size=1, stride=1, padding=0)
        self.latlayer3 = None  # nn.Conv3d(64, 256, kernel_size=1, stride=1, padding=0)

        # Addendum layers to reduce channels before sum
        self.sumlayer1 = None
        self.sumlayer2 = None
        self.sumlayer3 = None

        # Semantic branch
        self.conv2_3d_p5 = None
        self.conv2_3d_p4 = None
        self.conv2_3d_p3 = None
        self.conv2_3d_p2 = None
        self.iam_joking(first_batch, not opt.no_cuda)

        self.semantic_branch_2d = nn.Conv2d(256, 128, kernel_size=3, stride=1, padding=1)
        self.conv2_2d = nn.Conv2d(256, 256, kernel_size=3, stride=1, padding=1)
        # self.conv3 = nn.Conv2d(128, self.num_classes, kernel_size=1, stride=1, padding=0)
        self.conv3 = nn.Conv2d(128, 64, kernel_size=1, stride=1, padding=0)
        # opt.sample_duration is the number of samples taken from the time series
        self.conv4out = nn.Conv2d(64, opt.sample_duration, kernel_size=3, stride=1, padding=1)
        self.conv5out = nn.Conv2d(opt.sample_duration, self.num_classes, kernel_size=3, stride=1, padding=1)
        # num_groups, num_channels
        self.gn1 = nn.GroupNorm(128, 128)
        self.gn2 = nn.GroupNorm(256, 256)


    def iam_joking(self, x, use_cuda):
        low_level_features = self.back_bone(x)
        c1 = low_level_features[0]
        c2 = low_level_features[1]
        c3 = low_level_features[2]
        c4 = low_level_features[3]
        c5 = low_level_features[4]

        # Top layer
        self.toplayer = nn.Conv3d(c5.size()[1], c5.size()[1], kernel_size=1, stride=1, padding=0)  # Reduce channels
        # Lateral layers
        self.latlayer1 = nn.Conv3d(c4.size()[1], c4.size()[1], kernel_size=1, stride=1, padding=0)
        self.latlayer2 = nn.Conv3d(c3.size()[1], c3.size()[1], kernel_size=1, stride=1, padding=0)
        self.latlayer3 = nn.Conv3d(c2.size()[1], c2.size()[1], kernel_size=1, stride=1, padding=0)

        # Addendum layers to reduce channels
        self.sumlayer1 = nn.Conv3d(c5.size()[1], c4.size()[1], kernel_size=1, stride=1, padding=0)  # Reduce channels
        self.sumlayer2 = nn.Conv3d(c4.size()[1], c3.size()[1], kernel_size=1, stride=1, padding=0)
        self.sumlayer3 = nn.Conv3d(c3.size()[1], c2.size()[1], kernel_size=1, stride=1, padding=0)

        if use_cuda:
            self.toplayer = self.toplayer.cuda()
            self.latlayer1 = self.latlayer1.cuda()
            self.latlayer2 = self.latlayer2.cuda()
            self.latlayer3 = self.latlayer3.cuda()
            self.sumlayer1 = self.sumlayer1.cuda()
            self.sumlayer2 = self.sumlayer2.cuda()
            self.sumlayer3 = self.sumlayer3.cuda()

        # Top-down
        p5 = self.toplayer(c5)
        p4 = self._upsample_add(self.sumlayer1(p5), self.latlayer1(c4))
        p3 = self._upsample_add(self.sumlayer2(p4), self.latlayer2(c3))
        p2 = self._upsample_add(self.sumlayer3(p3), self.latlayer3(c2))

        # Smooth layers
        # self.smooth1 = nn.Conv3d(p4.size()[1], 256, kernel_size=3, stride=1, padding=1)
        # self.smooth2 = nn.Conv3d(p3.size()[1], 256, kernel_size=3, stride=1, padding=1)
        # self.smooth3 = nn.Conv3d(p2.size()[1], 256, kernel_size=3, stride=1, padding=1)
        # if use_cuda:
        #     self.smooth1 = self.smooth1.cuda()
        #     self.smooth2 = self.smooth2.cuda()
        #     self.smooth3 = self.smooth3.cuda()

        # Smooth
        # p4 = self.smooth1(p4)
        # p3 = self.smooth2(p3)
        # p2 = self.smooth3(p2)

        # fare il conto in modo tale che la dimensione c diventi 1
        self.conv2_3d_p5 = nn.Conv3d(p5.size()[1], 256, kernel_size=(p5.size()[2] + 2, 3, 3), stride=1, padding=1)
        self.conv2_3d_p4 = nn.Conv3d(p4.size()[1], 256, kernel_size=(p4.size()[2] + 2, 3, 3), stride=1, padding=1)
        self.conv2_3d_p3 = nn.Conv3d(p3.size()[1], 128, kernel_size=(p3.size()[2] + 2, 3, 3), stride=1, padding=1)
        self.conv2_3d_p2 = nn.Conv3d(p2.size()[1], 128, kernel_size=(p2.size()[2] + 2, 3, 3), stride=1, padding=1)

    def _upsample3d(self, x, d, h, w):
        return F.interpolate(x, size=(d, h, w), mode='trilinear', align_corners=True)

    def _upsample2d(self, x, h, w):
        return F.interpolate(x, size=(h, w), mode='bilinear', align_corners=True)

    def _make_layer(self, Bottleneck, planes, num_blocks, stride):
        strides = [stride] + [1] * (num_blocks - 1)
        layers = []
        for stride in strides:
            layers.append(Bottleneck(self.in_planes, planes, stride))
            self.in_planes = planes * Bottleneck.expansion
        return nn.Sequential(*layers)

    def _upsample_add(self, x, y):
        '''Upsample and add two feature maps.
        Args:
          x: (Variable) top feature map to be upsampled.
          y: (Variable) lateral feature map.
        Returns:
          (Variable) added feature map.
        Note in PyTorch, when input size is odd, the upsampled feature map
        with `F.upsample(..., scale_factor=2, mode='nearest')`
        maybe not equal to the lateral feature map size.
        e.g.
        original input size: [N,_,15,15] ->
        conv2d feature map size: [N,_,8,8] ->
        upsampled feature map size: [N,_,16,16]
        So we choose bilinear upsample which supports arbitrary output sizes.
        '''
        _, _, D, H, W = y.size()
        return F.interpolate(x, size=(D, H, W), mode='trilinear', align_corners=True) + y

    def forward(self, x):
        # Bottom-up using backbone
        low_level_features = self.back_bone(x)
        # c1 = low_level_features[0]
        c2 = low_level_features[1]
        c3 = low_level_features[2]
        c4 = low_level_features[3]
        c5 = low_level_features[4]

        # Top-down
        p5 = self.toplayer(c5)
        p4 = self._upsample_add(
            torch.relu(self.sumlayer1(p5)), torch.relu(self.latlayer1(c4)))  # p5 interpolation to the size of c4
        p3 = self._upsample_add(
            torch.relu(self.sumlayer2(p4)), torch.relu(self.latlayer2(c3)))
        p2 = self._upsample_add(
            torch.relu(self.sumlayer3(p3)), torch.relu(self.latlayer3(c2)))

        # Smooth
        # p4 = F.relu(self.smooth1(p4))
        # p3 = F.relu(self.smooth2(p3))
        # p2 = F.relu(self.smooth3(p2))

        # Semantic
        _, _, _, h, w = p2.size()
        # 256->256
        s5 = self.conv2_3d_p5(p5)
        s5 = torch.squeeze(s5, 2)  # squeeze only dim 2 to avoid to remove the batch dimension
        s5 = self._upsample2d(torch.relu(self.gn2(s5)), h, w)
        # 256->256 [32, 256, 24, 24]
        s5 = self._upsample2d(torch.relu(self.gn2(self.conv2_2d(s5))), h, w)
        # 256->128 [32, 128, 24, 24]
        s5 = self._upsample2d(torch.relu(self.gn1(self.semantic_branch_2d(s5))), h, w)

        # 256->256 p4:[32, 256, 4, 6, 6] -> s4:[32, 256, 1, 6, 6]
        s4 = self.conv2_3d_p4(p4)
        s4 = torch.squeeze(s4, 2)  # s4:[32, 256, 6, 6]
        s4 = self._upsample2d(torch.relu(self.gn2(s4)), h, w)  # s4:[32, 256, 24, 24]
        # 256->128  s4:[32, 128, 24, 24]
        s4 = self._upsample2d(torch.relu(self.gn1(self.semantic_branch_2d(s4))), h, w)

        # 256->128
        s3 = self.conv2_3d_p3(p3)
        s3 = torch.squeeze(s3, 2)
        s3 = self._upsample2d(torch.relu(self.gn1(s3)), h, w)

        s2 = self.conv2_3d_p2(p2)
        s2 = torch.squeeze(s2, 2)
        s2 = self._upsample2d(torch.relu(self.gn1(s2)), h, w)

        out = self._upsample2d(self.conv3(s2 + s3 + s4 + s5), 2 * h, 2 * w)
        # introducing MSELoss on NDVI signal
        out_cai = torch.sigmoid(self.conv4out(out))  # for Class Activation Interval
        out_cls = self.conv5out(out_cai)  # for Classification

        return out_cai, out_cls


'''
    def _init_weights(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                m.weight.data.normal_(0, sqrt(2. / n))
                if m.bias is not None:
                    m.bias.data.zero_()
            elif isinstance(m, nn.BatchNorm2d):
                m.weight.data.fill_(1)
                m.bias.data.zero_()
            elif isinstance(m, nn.GroupNorm):
                m.weight.data.fill_(1)
                m.bias.data.zero_()
'''
if __name__ == "__main__":
    opt = parse_opts()
    os.environ['CUDA_VISIBLE_DEVICES'] = opt.gpu_id

    traindataset = RandomDataset()
    # nclasses = len(traindataset.classes)
    traindataloader = torch.utils.data.DataLoader(traindataset, batch_size=16, shuffle=True, num_workers=0)
    first_input_batch, _ = next(iter(traindataloader))
    print("input shape:", first_input_batch.shape)
    model = FPN(opt, first_input_batch)
    optimizer = torch.optim.Adam(model.parameters(), lr=1e-3)
    loss = torch.nn.NLLLoss()

    for iteration, data in enumerate(traindataloader):
        optimizer.zero_grad()

        input, target = data

        if torch.cuda.is_available():
            input = input.cuda()
            target = target.cuda()

        output = model.forward(input)
        l = loss(output, target)

        print("output:", output.size())
        print("loss:", l.data.cpu().numpy())

        l.backward()
        optimizer.step()

    # input = torch.rand(1,3,512,1024)
    # input = torch.rand(1,3,48,48)
    # output = model(input)
    # print(output.size())
