import os
import sys

import json

import numpy
import torch

from models.FPN import FPN
from models.FPN_NDVI import FPN_NDVI
from opts import parse_opts
from utils.functional import adjust_learning_rate, adjust_classes_weights
from utils.meter import AverageValueMeter
from utils.metrics import ConfusionMatrix
from utils.ndvi_signature import write_signatures
from utils.sentinel_dataset import RandomDataset, SentinelDataset
from utils.loss import SegmentationLosses
from utils.snapshot import resume, save

opt = parse_opts()
os.environ['CUDA_VISIBLE_DEVICES'] = opt.gpu_id
if opt.no_cuda:
    os.environ['CUDA_VISIBLE_DEVICES'] = ""



# compute weights
# https://towardsdatascience.com/handling-class-imbalanced-data-using-a-loss-specifically-made-for-it-6e58fd65ffab
def compute_train_weights(train_loader, opt):
    beta = 0.9
    samples_per_cls = torch.zeros(opt.n_classes)
    for batch_idx, data in enumerate(train_loader):
        inputs, targets, _, _, _ = data
        for cls in range(opt.n_classes):
            samples_per_cls[cls] += torch.sum(targets == cls)
    max_occ = torch.max(samples_per_cls)
    weights = torch.FloatTensor(max_occ / samples_per_cls)
    # max_occ = torch.max(weights)
    # weights = torch.FloatTensor(weights / max_occ)
    if torch.cuda.is_available():
        weights = weights.cuda()

    return weights


def train_epoch(dataloader, network, optimizer, loss, ep, opt, loss_cls, cls_weights=None):
    num_processed_samples = 0
    num_train_samples = len(dataloader.dataset)

    conf_mat_metrics = ConfusionMatrix(labels, ignore_class=opt.ignore_index)
    loss_measure = AverageValueMeter()
    var_learning_rate = opt.learning_rate
    for iteration, data in enumerate(dataloader):
        # with torch.no_grad():
        if opt.optimizer == 'sgd':
            var_learning_rate = adjust_learning_rate(ep, iteration,  len(dataloader),
                                      opt.n_epochs, opt.learning_rate, lr_decay='cos')
            for param_group in optimizer.param_groups:
                param_group['lr'] = var_learning_rate
        if cls_weights is not None:
            loss_cls.weight = adjust_classes_weights(ep, iteration, len(dataloader),
                                      opt.n_epochs, cls_weights, descending=False)

        optimizer.zero_grad()

        input, target, target_ndvi, _, _ = data
        num_processed_samples += len(input)
        if torch.cuda.is_available():
            input = input.cuda()
            target = target.cuda()
            target_ndvi = target_ndvi.cuda()

        if opt.loss == 'batch':
            output = network.forward(input)
            samples_per_cls = torch.stack([(target == x_u).sum() for x_u in range(opt.n_classes)])
            l = loss(output, target, samples_per_cls)
        elif opt.loss == 'ndvi':
            out_ndvi, output = network.forward(input)
            samples_per_cls = None
            if opt.loss_weights:
                samples_per_cls = torch.stack([(target == x_u).sum() for x_u in range(opt.n_classes)])
            l = loss(out_ndvi, output, target, target_ndvi, samples_per_cls, weight_mse=1.0)
        else:
            output = network.forward(input)
            l = loss(output, target)

        l.backward()
        optimizer.step()

        with torch.no_grad():
            pred = torch.argmax(output, dim=1).squeeze(1)

            metrics_v, metrics_scalar = conf_mat_metrics(pred, target)
            str_metrics = ''.join(['%s| %f | ' % (key, value) for (key, value) in metrics_scalar.items()])
            loss_measure.add(l.item())
            str_metrics += 'loss| %f | ' % loss_measure.mean

            train_info = 'Train on | {} | Epoch| {} | [{}/{} ({:.0f}%)] | lr| {:.5f} | {}    '.format(
                dataloader.dataset.name, ep, num_processed_samples, num_train_samples,
                100. * (iteration + 1) / len(dataloader), var_learning_rate, str_metrics)
            sys.stdout.write('\r' + train_info)

    print()
    with open(os.path.join(opt.result_path, opt.result_train), 'a') as f:
        f.write(train_info + '\n')


def test_epoch(dataloader, network, loss, opt):
    num_processed_samples = 0
    num_test_samples = len(dataloader.dataset)
    conf_mat_metrics = ConfusionMatrix(labels, ignore_class=opt.ignore_index)
    loss_measure = AverageValueMeter()

    with torch.no_grad():
        for iteration, data in enumerate(dataloader):

            input, target, target_ndvi, _, _ = data
            num_processed_samples += len(input)
            if torch.cuda.is_available():
                input = input.cuda()
                target = target.cuda()
                target_ndvi = target_ndvi.cuda()

            if opt.loss == 'batch':
                output = network.forward(input)
                samples_per_cls = torch.stack([(target == x_u).sum() for x_u in range(opt.n_classes)])
                l = loss(output, target, samples_per_cls)
            elif opt.loss == 'ndvi':
                out_ndvi, output = network.forward(input)
                samples_per_cls = None
                if opt.loss_weights:
                    samples_per_cls = torch.stack([(target == x_u).sum() for x_u in range(opt.n_classes)])
                l = loss(out_ndvi, output, target, target_ndvi, samples_per_cls)
            else:
                output = network.forward(input)
                l = loss(output, target)

            pred = torch.argmax(output, dim=1).squeeze(1)
            metrics_v, metrics_scalar = conf_mat_metrics(pred, target)
            str_metrics = ''.join(['%s| %f | ' % (key, value) for (key, value) in metrics_scalar.items()])
            loss_measure.add(l.item())
            str_metrics += 'loss| %f | ' % loss_measure.mean
            test_info = 'Test on | {} | Epoch | {} | [{}/{} ({:.0f}%)] | {}  '.format(
                dataloader.dataset.name, epoch, num_processed_samples,
                num_test_samples, 100. * (iteration + 1) / len(dataloader),
                str_metrics)
            sys.stdout.write('\r' + test_info)

        is_best = metrics_scalar['OA'] > best_test_acc
        best = '  **best result' if is_best else '         '
        test_info += best

        sys.stdout.write('\r' + test_info + '\n')
        with open(os.path.join(opt.result_path, opt.result_validation), 'a') as f:
            f.write(test_info + '\n')

        if is_best:
            cls_names = numpy.array(traindataset.classes)[conf_mat_metrics.get_labels()]
            with open(os.path.join(opt.result_path, "per_class_metrics.txt"), 'w') as f:
                f.write('classes:\n' + numpy.array2string(cls_names) + '\n')
                for k, v in metrics_v.items():
                    f.write(k + '\n')
                    if len(v.shape) == 1:
                        for ki, vi in zip(cls_names, v):
                            f.write("%.2f" % vi + '\t' + ki + '\n')
                    elif len(v.shape) == 2:  # confusion matrix
                        num_gt = numpy.sum(v, axis=1)
                        f.write('\n'.join(
                            [''.join(['{:10}'.format(item) for item in row]) + '  ' + lab + '(%d)' % tot
                             for row, lab, tot in zip(v, cls_names, num_gt)]))
                        f.write('\n')

        return metrics_scalar['OA']  # test_acc


def test_only(dataloader, network, loss, opt, epoch, set_name):
    num_processed_samples = 0
    num_test_samples = len(dataloader.dataset)
    conf_mat_metrics = ConfusionMatrix(labels, ignore_class=opt.ignore_index)
    loss_measure = AverageValueMeter()

    with torch.no_grad():
        for iteration, data in enumerate(dataloader):

            input, target, target_ndvi, dates, patch_id = data

            num_processed_samples += len(input)
            if torch.cuda.is_available():
                input = input.cuda()
                target = target.cuda()
                target_ndvi = target_ndvi.cuda()

            if opt.loss == 'batch':
                output = network.forward(input)
                samples_per_cls = torch.stack([(target == x_u).sum() for x_u in range(opt.n_classes)])
                l = loss(output, target, samples_per_cls)
            elif opt.loss == 'ndvi':
                out_ndvi, output = network.forward(input)
                samples_per_cls = None
                if opt.loss_weights:
                    samples_per_cls = torch.stack([(target == x_u).sum() for x_u in range(opt.n_classes)])
                l = loss(out_ndvi, output, target, target_ndvi, samples_per_cls)
                if iteration % 100 == 0:
                    write_signatures(model.module, target, output, target_ndvi, out_ndvi, dates, patch_id, opt, set_name)
            else:
                output = network.forward(input)
                l = loss(output, target)

            pred = torch.argmax(output, dim=1).squeeze(1)
            metrics_v, metrics_scalar = conf_mat_metrics(pred, target)
            str_metrics = ''.join(['%s| %f | ' % (key, value) for (key, value) in metrics_scalar.items()])
            loss_measure.add(l.item())
            str_metrics += 'loss| %f | ' % loss_measure.mean
            test_info = 'Test on | {} | Epoch | {} | [{}/{} ({:.0f}%)] | {}  '.format(
                dataloader.dataset.name, epoch, num_processed_samples,
                num_test_samples, 100. * (iteration + 1) / len(dataloader),
                str_metrics)
            sys.stdout.write('\r' + test_info)

        cls_names = numpy.array(traindataset.classes)[conf_mat_metrics.get_labels()]
        sys.stdout.write('\r' + test_info + '\n')
        with open(os.path.join(opt.result_path, set_name + "_per_class_metrics.txt"), 'w') as f:
            f.write('classes:\n' + numpy.array2string(cls_names) + '\n')
            sys.stdout.write('classes:\n' + numpy.array2string(cls_names) + '\n')
            for k, v in metrics_v.items():
                sys.stdout.write('\n' + k + '\n')
                f.write('\n' + k + '\n')
                if len(v.shape) == 1:
                    for ki, vi in zip(cls_names, v):
                        sys.stdout.write("%.2f" % vi + '\t' + ki + '\n')
                        f.write("%.2f" % vi + '\t' + ki + '\n')
                elif len(v.shape) == 2:  # confusion matrix
                    num_gt = numpy.sum(v, axis=1)
                    sys.stdout.write('\n'.join(
                        [''.join(['{:10}'.format(item) for item in row]) + '  ' + lab + '(%d)' % tot
                         for row, lab, tot in zip(v, cls_names, num_gt)]))
                    f.write('\n'.join(
                        [''.join(['{:10}'.format(item) for item in row]) + '  ' + lab + '(%d)' % tot
                         for row, lab, tot in zip(v, cls_names, num_gt)]))
                    sys.stdout.write('\n')
                    f.write('\n')

        if opt.loss == 'ndvi':
            print("\nClass Activation Interval saved in:", opt.result_path)


if __name__ == "__main__":

    best_test_acc = 0

    if not os.path.exists(opt.result_path):
        os.makedirs(opt.result_path)
    with open(os.path.join(opt.result_path, "commandline_args.txt"), 'w') as f:
        json.dump(opt.__dict__, f, indent=2)

    traindataset = SentinelDataset(opt.root_path, tileids="tileids/train_fold0.tileids", seqlength=opt.sample_duration)
    validationdataset = SentinelDataset(opt.root_path, tileids="tileids/test_fold0.tileids", seqlength=opt.sample_duration)
    opt.n_classes = len(traindataset.classes)
    labels = list(range(opt.n_classes))
    traindataloader = torch.utils.data.DataLoader(
        traindataset, batch_size=opt.batch_size, shuffle=True, num_workers=opt.workers)
    validationdataloader = torch.utils.data.DataLoader(
        validationdataset, batch_size=opt.batch_size, shuffle=False, num_workers=opt.workers)

    # input, target, target_ndvi
    first_input_batch, _, _, _, _ = next(iter(traindataloader))
    print("input shape:", first_input_batch.shape)

    if opt.loss == 'ndvi':
        model = FPN_NDVI(opt, first_input_batch)
    else:
        model = FPN(opt, first_input_batch)
    if torch.cuda.is_available():
        model = torch.nn.DataParallel(model).cuda()
    if opt.optimizer == 'adam':
        optimizer = torch.optim.Adam(model.parameters(),
                                     lr=opt.learning_rate,
                                     weight_decay=opt.weight_decay)
    elif opt.optimizer == 'sgd':
        optimizer = torch.optim.SGD(model.parameters(),
                                    lr=opt.learning_rate,
                                    momentum=opt.momentum,
                                    weight_decay=opt.weight_decay)

    # Define Criterion
    weights = None
    if not (opt.loss == 'batch' or opt.loss == 'ndvi') and opt.loss_weights:
        print('Computing weights per classes...')
        weights = compute_train_weights(traindataloader, opt)
        weights = torch.sqrt(weights)
        print("weights per classes:", weights)

    loss_cls = SegmentationLosses(weight=weights, cuda=not opt.no_cuda, ignore_index=opt.ignore_index)
    loss = loss_cls.build_loss(mode=opt.loss)

    start_epoch = 0
    if opt.resume_path:  # Empty strings are considered false
        print('trying to resume previous saved model...')
        state = resume(opt.resume_path, model=model, optimizer=optimizer)

        if "epoch" in state.keys():
            start_epoch = state["epoch"]
            best_test_acc = state['best_test_acc']

    if opt.test_only:
        testdataset = SentinelDataset(opt.root_path, tileids="tileids/eval.tileids", seqlength=opt.sample_duration)
        testdataloader = torch.utils.data.DataLoader(
            testdataset, batch_size=opt.batch_size, shuffle=False, num_workers=opt.workers)
        test_only(testdataloader, model, loss, opt, start_epoch, "eval")
        test_only(validationdataloader, model, loss, opt, start_epoch, "test_fold0")
        test_only(traindataloader, model, loss, opt, start_epoch, "train_fold0")
        exit(0)

    for epoch in range(start_epoch, opt.n_epochs):
        train_epoch(traindataloader, model, optimizer, loss, epoch, opt, loss_cls, cls_weights=None)
        val_acc = test_epoch(validationdataloader, model, loss, opt)

        is_best = val_acc > best_test_acc
        if is_best:
            epochs_best_acc = epoch
            best_test_acc = val_acc
            if opt.result_path:
                checkpoint_name = os.path.join(opt.result_path, "best_model.pth")
                save(checkpoint_name, model, optimizer,
                     epoch=epoch, best_test_acc=best_test_acc)

